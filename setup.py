#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Install script for package"""

from distutils.core import setup


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths


setup(
    name='gw_compare',
    description='Utilities and libraries for waveform reconstruction comparisons and residuals reconstructions',
    version='3.0',
    author='James Alexander Clark, Sudarshan Ghonge',
    author_email='james.clark@ligo.org, sudarshan.ghonge@ligo.org',
    url='https://git.ligo.org/james-clark/gw-compare',
    scripts=['scripts/gwcomp_reclal.py',
             'scripts/gwcomp_residuals.py', 'scripts/gwcomp_testgr.py',
             'scripts/gwcomp_bw_li_inj.py','scripts/gwcomp_catalog.py'],
    packages=['gw_compare'],
    license='GPL',
    long_description=open('README.md').read()
    # packages=['gw_compare']
)
